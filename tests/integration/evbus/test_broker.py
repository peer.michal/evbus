import asyncio
import queue

import evbus.evbus.broker as broker


async def test_init_synchronous(hub):
    assert isinstance(hub.evbus.BUS, queue.Queue)
    bus = await hub.evbus.broker.init()
    assert isinstance(bus, asyncio.Queue)


async def test_init_asynchronous(hub):
    bus = await hub.evbus.broker.init()
    assert isinstance(bus, asyncio.Queue)


async def test_put(hub):
    await hub.evbus.broker.put("body")
    event = await hub.evbus.broker.get()
    assert event.body == b'"body"'


async def test_put_nowait(hub):
    hub.evbus.broker.put_nowait("body")
    event = await hub.evbus.broker.get()
    assert event.body == b'"body"'


async def test_propagate(hub):
    contexts = {"internal": [{"default": {"routing_key": "key"}}]}
    event = broker.Event(profile="default", body="body")
    await hub.evbus.broker.propagate(contexts, event)
    body = await hub.ingress.internal.QUEUE["key"].get()
    assert body == "body"


async def test_high_load_before_init(hub):
    contexts = {"internal": [{"default": {"routing_key": "key"}}]}

    for index in range(2000):
        hub.evbus.broker.put_nowait(index)

    t = hub.pop.Loop.create_task(hub.evbus.init.start(contexts))
    hub.evbus.init.join()

    # This pushes terminal event in the queue first
    await hub.evbus.init.stop()

    # Now push more events
    # These two events will go back in the queue behind terminal event
    await hub.evbus.broker.put(2001)
    hub.evbus.broker.put_nowait(2002)

    await t

    # Get the internal queue that should have been all the events pushed via log message
    queue = hub.ingress.internal.QUEUE["key"]
    assert queue.qsize() == 2002

    indexes = []
    while not queue.empty():
        message = await queue.get()
        indexes.append(int(message.decode()))

    assert len(indexes) == 2002
    # make sure all items are sorted
    is_sorted = all(indexes[i] < indexes[i + 1] for i in range(len(indexes) - 1))
    assert is_sorted
