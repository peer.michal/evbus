from unittest import mock

import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="evbus")

    with mock.patch("sys.argv", ["evbus"]):
        hub.pop.config.load(["evbus", "acct", "rend"], cli="evbus")

    yield hub
