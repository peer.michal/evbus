.. _README:

.. include:: ../../README.rst

Integration
===========

There are three core steps to integrating evbus in your app.
1. Configure the serialize plugin
2. Start the evbus listener
3. Publish events
Here is the most basic implementation of those steps:

.. code-block:: python

    def main(hub):
        # Read configuration into hub.OPT
        hub.pop.config.load(["my_app", "evbus", "acct"], cli="my_app")

        # Configure the serializer that will be used to send messages
        hub.evbus.SERIAL_PLUGIN = hub.OPT.evbus.serial_plugin

        # Initialize the pop loop
        hub.pop.loop.create()

        # Run the loop function defined below
        hub.pop.Loop.run_until_complete(hub.my_app.init.loop())


    async def loop(hub):

        # Collect ingress profiles from acct
        profiles = await hub.evbus.acct.profiles(
            acct_file=hub.OPT.acct.acct_file,
            acct_key=hub.OPT.acct.acct_key,
        )

        # Start the listener in it's own task
        task = hub.pop.Loop.create_task(hub.evbus.init.start(profiles))

        # Wait for the event bus to start
        await hub.evbus.init.join()

        # Initialize your app
        await hub.my_app.my_plugin.run()

        # Put a StopIteration event on the broker queue
        await hub.evbus.init.stop()

        # Wait for the broker queue to clear out and return
        await task
